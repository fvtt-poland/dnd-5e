Hooks.once('init', () => {

	if(typeof Babele !== 'undefined') {

		Babele.get().register({
			module: 'dnd5e_pl',
			lang: 'pl',
			dir: 'compendium'
		});
	}
});
