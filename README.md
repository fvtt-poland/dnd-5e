**Opis**

Moduł ten to tłumaczenie systemu DnD5e do aplikacji Foundry VTT na język polski. Przetłumaczona została karta postaci i związane z nią opcje. W celu zapewnienia jak najlepszych wrażeń, konieczna była zmiana używanej czcionki, oryginalna nie zawiera polskich znaków i objęta jest licencją, w związku z czym edycja jej była niemożliwa.

Aby moduł działał poprawnie należy włączyć go w ustawieniach modułów bezpośrednio z poziomu menu gry.

Przetłumaczyłem również kompendium, niestety pracy jest sporo, dlatego nie wszystkie elementy są jeszcze spolszczone.

**Podzękowania**

Dziękuję moim graczom za pomoc w tłumaczeniu starych wersji. Dodatkowo dziękuję Tomaszowi Marchutowi za udostępnienie skryptu sortującego umiejętności w kolejności alfabetycznej oraz pomoc w weryfikacji wyglądu karty.

**Instalacja**
1. Skopiować link: https://gitlab.com/fvtt-poland/dnd-5e/-/raw/main/dnd5e_pl/module.json?inline=false
2. Wkleić zawartość schowka w pole URL Manifestu, w sekcji instalacji nowych modułów
3. Zainstalować moda z linku
4. Włączyć moda w opcjach modułów dostępnych z menu gry

**Informacje dodatkowe**

Spolszczenie aplikacji Foundry VTT dostępne jest tutaj:
https://gitlab.com/fvtt-poland/core


W przypadku sugestii, zgłaszania błędów można skontaktować się ze mną bezpośrednio pod adresem waaldii@outlook.com

If you have any questions, please contact me using email: waaldii@outlook.com
